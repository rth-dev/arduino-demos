/*
 *  project:  control shield demo
 * 
 *  date:     21.10.2017
 *  file:     control-shield-demo.ino
 *
 *  author:   rth.
 *  
 *  hardware: ControlShield (Rev_B)
 *
 */

//______switches_________________________________________
#define PIN_SW_AM       7   // auto/manual
#define PIN_SW_IE       6   // intern/extern
#define PIN_SW_LEFT     9   // left switch
#define PIN_SW_RIGHT    8   // right switch

//______leds_____________________________________________
#define PIN_LED_RED     5   // LED red
#define PIN_LED_GREEN   13  // LED green
#define PIN_LED_LEFT    11  // LED left switch
#define PIN_LED_RIGHT   10  // LED right switch

//______potis____________________________________________
#define PIN_POT_SPD     A4  // value shown on serial I/F
#define PIN_POT_P       A5  // value shown on serial I/F
#define PIN_POT_I       A6  // value shown on serial I/F
#define PIN_POT_D       A11 // value shown on serial I/F

//______globals__________________________________________
#define LED_BRIGHTNESS_MIN 5
int state_sw_am    = 0;
int state_sw_ie    = 0;
int state_sw_left  = 0;
int state_sw_right = 0;
int val_spd        = 0;
int val_p          = 0;
int val_i          = 0;
int val_d          = 0;
int val_pwm        = 0;
bool pwm_toggle    = 0;
unsigned long time_curr;
unsigned long time_last;
unsigned long time_diff;


/********************************************************/
void setup() {

  // serial interfaces
  Serial.begin(9600);   // usb
  Serial1.begin(9600);  // ttl
  
  // switches
  pinMode(PIN_SW_AM,    INPUT);
  pinMode(PIN_SW_IE,    INPUT);
  pinMode(PIN_SW_LEFT,  INPUT);
  pinMode(PIN_SW_RIGHT, INPUT);
  
  // leds
  pinMode(PIN_LED_RED,   OUTPUT);
  pinMode(PIN_LED_GREEN, OUTPUT);
  pinMode(PIN_LED_LEFT,  OUTPUT);
  pinMode(PIN_LED_RIGHT, OUTPUT);
  
  // potis
  pinMode(PIN_POT_SPD,  INPUT);
  pinMode(PIN_POT_D,    INPUT);
  pinMode(PIN_POT_I,    INPUT);
  pinMode(PIN_POT_D,    INPUT);

} // end-setup()


/********************************************************/
void loop() {

  //______read_inputs____________________________________
  state_sw_am    = digitalRead(PIN_SW_AM);
  state_sw_ie    = digitalRead(PIN_SW_IE);
  state_sw_left  = digitalRead(PIN_SW_LEFT);
  state_sw_right = digitalRead(PIN_SW_RIGHT);
  val_spd = analogRead(PIN_POT_SPD);
  val_p   = analogRead(PIN_POT_P);
  val_i   = analogRead(PIN_POT_I);
  val_d   = analogRead(PIN_POT_D);


  //______update_time____________________________________
  time_last = time_curr;
  time_curr = millis();
  time_diff += (time_curr - time_last);

  
  //______print_serial_message_&_leds_brightness__________
  if(time_diff > val_spd/4) {

    Serial.print("loop() | millis:"); Serial.print(time_curr);
    Serial.print("  diff:");  Serial.print(time_diff);
    Serial.print("  spd:");   Serial.print(val_spd);
    Serial.print("  p:");     Serial.print(val_p);
    Serial.print("  i:");     Serial.print(val_i);
    Serial.print("  d:");     Serial.print(val_d);
    Serial.print("  a/m:");   Serial.print(state_sw_am);
    Serial.print("  i/e:");   Serial.print(state_sw_ie);
    Serial.print("  left:");  Serial.print(state_sw_left);
    Serial.print("  right:"); Serial.print(state_sw_right);
    Serial.print("  pwm:");   Serial.print(val_pwm);
    Serial.println();
    time_diff = 0;    

    //______update_leds_brightness________________________
    if(!pwm_toggle) {
      val_pwm+=1;
    } else {
      val_pwm-=1;
    }
    
    if(val_pwm >= 255) {
      val_pwm = 255;
      pwm_toggle = 1;
    } else if(val_pwm <= LED_BRIGHTNESS_MIN) {
      val_pwm = LED_BRIGHTNESS_MIN;
      pwm_toggle = 0;
    }
    
  } // end-if


  //______update_leds_____________________________________
  if(!state_sw_am) {
    analogWrite(PIN_LED_RED, val_pwm);    // LED red: pwm value
  } else {
    digitalWrite(PIN_LED_RED, LOW);       // LED red: off
  }
  
  if(!state_sw_ie) {
    analogWrite(PIN_LED_GREEN, val_pwm);  // LED green: pwm value
  } else {
    digitalWrite(PIN_LED_GREEN, LOW);     // LED green: off
  }

  if(!state_sw_left) {
    analogWrite(PIN_LED_LEFT, val_pwm);   // LED left: pwm value
  }
  else {
    digitalWrite(PIN_LED_LEFT, LOW);      // LED left: off
  }
  
  if(!state_sw_right) {
    analogWrite(PIN_LED_RIGHT, val_pwm);  // LED right: pwm value
  }
  else {
    digitalWrite(PIN_LED_RIGHT, LOW);     // LED right: off
  }

} // end-loop()
